﻿function getpan(o){

	if (! $("#search").hasClass('loadb')){
		$("#search").addClass('loadb');
	}

	$.getJSON("/api/search/search?nostat=1&keyword="+o.value+"&r=" + (Math.floor(Math.random() * 100000)),
		function(data){
			$("#search").removeClass('loadb');
			if (data.status && data.status == 200) {
				if (data.res.length){
					
					megadiv(o.id+'_div');
					
					txt='';
					r = new RegExp("("+o.value+")","ig");
					for (i=0;i<data.res.length;i++){
						val = data.res[i][0];
						val = val.replace(r,"<b>$1</b>");
						val = val.split(',');
						val = val[0]+((val[1] != undefined) ? '<span>, '+val[1]+((val[2] != undefined) ? ', '+val[2] : '' )+'</span>' : '');
						txt+='<div onmouseover="chanhe(this,1);" onmouseout="chanhe(this,0);" onclick="load_main('+data.res[i][1]+');return false;">'+val+'</div>';
					}

					$("#"+o.id+'_div').html(txt).removeClass('hidden');
					
				} else {
					$("#"+o.id+'_div').addClass("hidden");
				}
			} else {
				error_message(o,data.error);
			}
		}
	);
}

function megadiv(id) {
	if (! $("#megadiv").length){
		$('<div id="megadiv"></div>').insertAfter("div.ftt-header");
		$("#megadiv").click(function(){
			$("#"+id).addClass("hidden");
			$(this).remove();
		});
	}
}

function setsearch(o)  {
	$("#suggest").val(o.innerHTML).focus();
}

function chanhe(obj,id)  {
	if (id == 1){obj.style.backgroundColor='#F7F7F7'}
	else {obj.style.backgroundColor=''}
}

function load_main(cid){

	if (! $("#search").hasClass('loadb')){
		$("#search").addClass('loadb');
	}
	$("#megadiv").click();

	$.getJSON("/api/search/lookup?cid="+cid+"&r=" + (Math.floor(Math.random() * 100000)),
		function(data){
			$("#search").removeClass('loadb');
			if (data.status && data.status == 200) {
				if (data.city != "") {
					$("#suggest").val(data.city)
				}
				
				
				if (data.people.length || data.tips.length){

				people = '';	
				for (i=0;i<data.people.length;i++){
					people+='<div class="ftt-tile">\
								<div class="ftt-tile__container">\
									<div class="ftt-tile__ava" style="background-image:url(\'/img/ava/'+data.people[i]["pic"]+'\')"></div>\
									<a href="" class="ftt-tile__name">'+data.people[i]["name"]+'</a>\
									<div class="ftt-tile__date">'+data.people[i]["date"]+'</div>\
									<div class="ftt-tile__place ftt-locator-icon">'+data.people[i]["city"]+'</div>\
								</div>\
							</div>';
					
				}
				$("#people").html(people);
				
				leaders = '';
				for (i=0;i<data.leaders.length;i++){
					leaders+='<div class="ftt-users__item">\
								<div class="ftt-users__ava" style="background-image:url(\''+data.leaders[i]["pic"]+'\')"></div>\
								<div class="ftt-users__title">';
					if (parseInt(data.leaders[i]["age"]) == 0){
						leaders+=data.leaders[i]["name"];
					} else {
						leaders+=data.leaders[i]["name"]+', <span>'+data.leaders[i]["age"]+'</span>';
					}
					leaders+='</div>\
							</div>';
					
				}
				$("div.ftt-users__container").html(leaders);
				
				tips = '';
				for (i=0;i<data.tips.length;i++){
					tips+='<div class="ftt-tile ftt-tile_tip">\
							<div class="ftt-tile__container">\
								<div class="ftt-tile__ava" style="background-image:url(\'/img/ava/'+data.tips[i]["pic"]+'\')"></div>\
								<div class="ftt-tile__name">';
					if (data.tips[i]["city"] == ""){
						tips+=data.tips[i]["name"];
					} else {
						tips+=data.tips[i]["name"]+', <span>'+data.tips[i]["city"]+'</span>';
					}
					tips+='</div>\
								<div class="ftt-tile__text">'+data.tips[i]["mes"]+'</div>\
								<div class="ftt-tile__date">'+data.tips[i]["city"]+'</div>\
								<div class="ftt-tile__label ftt-tile__label_question">';
					if (parseInt(data.tips[i]["tip"]) == 0){
						tips+=FLang["Question"];
					} else {
						tips+=FLang["Tip"];
					}
					tips+='</div>\
								<div class="ftt-tile__share"></div>\
							</div>\
						</div>';
				}
				$("#tips").html(tips);

				
					
				/*	megadiv(o.id+'_div');
					
					txt='';
					r = new RegExp("("+o.value+")","ig");
					for (i=0;i<data.res.length;i++){
						val = data.res[i][0];
						val = val.replace(r,"<b>$1</b>");
						oncli = 'after_cid_search('+data.res[i][1]+');return false;';
						if (action) {
							oncli = 'loadforedit(this, '+data.res[i][1]+');return false;';
						}
						
						txt+='<div onmouseover="chanhe(this,1);" onmouseout="chanhe(this,0);" onclick="'+oncli+'">'+val+', <span class="small">'+data.res[i][1]+'</span></div>';
					}


					$("#"+o.id+'_div').html(txt).css(Sizes).removeClass('hidden');
					*/
				} else {
					//$("#"+o.id+'_div').addClass("hidden");
				}
			} else {
				//error_message(o,data.error);
			}
		}
	);
}


function aut(id) {
	$.post("/api/v2/auth", "&key="+id+"&r=" + (Math.floor(Math.random() * 100000)),
		function(data){
			if (data.status && data.status == 200) {
				if (parseInt(data.id) > 0){
					window.location.replace("/");
				} else {
					error_message(o, 'auth error');
				}
			} else {
				error_message(o, data.error);
			}
		}, "json"
	);
}